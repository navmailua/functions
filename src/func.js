const getSum = (str1, str2) => {
  if(typeof str1 === 'string' && typeof str2 === 'string' && !isNaN(str1) && !isNaN(str2))
    return (Number(str1)+Number(str2)).toString();
  return false;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
      let postCnt = 0;
      let comCnt = 0;
      listOfPosts.forEach(element => {
        if(element.author == authorName)
          postCnt++;
        if(element.hasOwnProperty('comments'))
        {
          element.comments.forEach(com => {
            if(com.author == authorName)
              comCnt++;
          });  
        }
      });
  return 'Post:'+postCnt+',comments:'+comCnt;
}

const tickets=(people)=> {
  let vasyacash=0;
  for(let value of people)
  {
    if(value-vasyacash<=25)
      vasyacash+=25;
    else return "NO";
  }
  return "YES";
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
